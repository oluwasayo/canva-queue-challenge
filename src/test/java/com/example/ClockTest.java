package com.example;

import org.junit.After;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * @author oladeji
 */
public class ClockTest {

  @After
  public void tearDown() {
    Clock.unfreeze();
  }

  @Test
  public void testNow_frozen() throws Exception {
    final LocalDateTime frozenTime = Clock.freeze();
    assertTrue(Clock.isFrozen());
    assertEquals(frozenTime, Clock.now()); // May give false positives.
  }

  @Test
  public void testFreeze() throws Exception {
    Clock.freeze();
    assertTrue(Clock.isFrozen());
  }

  @Test
  public void testFreeze_withTime() throws Exception {
    Clock.freeze(LocalDateTime.MIN);
    assertEquals(LocalDateTime.MIN, Clock.now());
  }

  @Test
  public void testUnfreeze() throws Exception {
    Clock.freeze(LocalDateTime.MIN);
    Clock.unfreeze();
    assertNotEquals(LocalDateTime.MIN, Clock.now());
  }
}