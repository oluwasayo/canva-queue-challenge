package com.example;

import com.amazonaws.util.IOUtils;
import org.junit.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.LocalDateTime.parse;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class FileQueueServiceTest extends AbstractLocalQueueTest {

  private static final String BASE_DIR = "file-queue-base";
  private static long VISIBILITY_TIMEOUT = 60;

  private static FileQueueService queueService;

  @BeforeClass
  public static void setUpClass() {
    queueService = new FileQueueService(BASE_DIR, VISIBILITY_TIMEOUT);
    Paths.get(BASE_DIR).toFile().mkdir();
  }

  @Before
  public void setUp() {
    Arrays.stream(Paths.get(BASE_DIR).toFile().listFiles()).forEach(e -> {
      if (e.isDirectory()) Arrays.stream(e.listFiles()).forEach(File::delete);
      e.delete();
    });
  }

  @After
  public void tearDown() {
    Clock.unfreeze();
  }

  @Test
  public void testFromRecord() {
    System.out.println("fromRecord");

    String record = "1##1234##2016-07-08T02:15:30##true##2016-07-08T02:10:30##Testing microphone";
    Message expResult = new Message("1", "1234", "Testing microphone",
        parse("2016-07-08T02:15:30", DateTimeFormatter.ISO_DATE_TIME), true,
        parse("2016-07-08T02:10:30", DateTimeFormatter.ISO_DATE_TIME));

    Message result = FileQueueService.fromRecord(record);
    assertEquals(expResult, result);
    assertEquals(expResult.getQueueId(), result.getQueueId());
    assertEquals(expResult.getMessageId(), result.getMessageId());
    assertEquals(expResult.getReadDate(), result.getReadDate());
    assertEquals(expResult.getText(), result.getText());
    assertEquals(expResult.hasExpired(), result.hasExpired());
    assertEquals(expResult.hashCode(), result.hashCode());
    assertEquals(expResult.isVisible(), result.isVisible());
    assertEquals(expResult.toString(), result.toString());
  }

  @Test
  public void testToRecord() {
    Message message = new Message("1", "1234", "Testing microphone",
        parse("2016-07-08T02:15:30", DateTimeFormatter.ISO_DATE_TIME), true,
        parse("2016-07-08T02:10:30", DateTimeFormatter.ISO_DATE_TIME));
    String expResult = "1##1234##2016-07-08T02:15:30##true##2016-07-08T02:10:30##Testing microphone";
    assertEquals(expResult, FileQueueService.toRecord(message));
  }

  @Test
  public void testPush_withExpiry() {
    System.out.println("push_withExpiry");

    final String QUEUE_ID = "1";

    assertTrue(isEmpty(QUEUE_ID));
    queueService.push(QUEUE_ID, "Testing microphone", Clock.now().plusMinutes(10));
    assertFalse(isEmpty(QUEUE_ID));
    assertTrue(queueService.getMessagesFile(QUEUE_ID).exists());
    assertEquals(1, lineCount(queueService.getMessagesFile(QUEUE_ID)));
  }

  private boolean isEmpty(String QUEUE_ID) {
    return queueService.getQueueDir(QUEUE_ID).list((dir, name) -> name.charAt(0) != '.').length == 0;
  }

  @Test
  public void testPush_multipleConcurrentProducers() {
    System.out.println("push_multipleConcurrentProducers");

    ExecutorService threadPool = Executors.newFixedThreadPool(10);
    List<Future> futures = new ArrayList<>();
    Random random = new Random();
    for (int a = 1; a <= 50; a++) {
      final String queueId = Integer.toString(1 + random.nextInt(10));
      futures.add(threadPool.submit(() -> queueService.push(queueId, "Testing mic", 3600000)));
    }

    futures.stream().forEach(e -> {
      try {
        e.get();
      } catch (InterruptedException | ExecutionException ex) {
        throw new RuntimeException("Error waiting for multiple producers to complete.", ex);
      }
    });

    int messageCount = 0;
    for (String queueId : Paths.get(BASE_DIR).toFile().list((dir, name) -> name.charAt(0) != '.')) {
      messageCount += lineCount(queueService.getMessagesFile(queueId));
    }
    assertEquals(50, messageCount);
  }

  @Test
  public void testDelete() {
    System.out.println("delete");

    queueService.push("1", "Testing microphone1", 3600000);
    final Message message = queueService.pull("1");

    assertNotNull(message);
    assertNull(queueService.pull("1"));
    assertEquals(1, lineCount(queueService.getMessagesFile("1")));

    queueService.delete(message.getQueueId(), message.getMessageId());
    assertEquals(0, lineCount(queueService.getMessagesFile("1")));
  }

  @Test
  public void testDelete_invalidMessage() throws IOException {
    System.out.println("delete_invalidMessage");

    queueService.push("1", "Testing microphone1", 3600000);
    final Message message = queueService.pull("1");
    assertNotNull(message);
    assertNull(queueService.pull("1"));

    String beforeDelete = IOUtils.toString(new FileInputStream(queueService.getMessagesFile("1")));
    queueService.delete(message.getQueueId(), "my random message id");
    String afterDelete = IOUtils.toString(new FileInputStream(queueService.getMessagesFile("1")));

    assertEquals(beforeDelete, afterDelete);
  }

  @Test
  public void testAcknowledgement() {
    System.out.println("testAcknowledgement");

    queueService.push("1", "msg1", 3600000);
    queueService.push("1", "msg2", 3600000);
    queueService.push("1", "msg3", 3600000);
    queueService.push("1", "msg4", 3600000);

    Clock.freeze();
    assertEquals("msg1", queueService.pull("1").getText());

    Clock.freeze(Clock.now().plusSeconds(1));
    assertEquals("msg2", queueService.pull("1").getText());

    Clock.freeze(Clock.now().plusSeconds(VISIBILITY_TIMEOUT + 1));

    assertEquals("msg1", queueService.pull("1").getText());
    assertEquals("msg2", queueService.pull("1").getText());
  }

  @Test
  public void testLocking() throws Exception {
    final File HANDLE = queueService.getLockHandle("1");
    queueService.lock(HANDLE);
    assertTrue(queueService.getLockHandle("1").exists());
    queueService.unlock(HANDLE);
    assertFalse(queueService.getLockHandle("1").exists());
  }

  @Override
  protected QueueService getQueueService() {
    return queueService;
  }

  private long lineCount(File file) {
    if (!file.exists()) return 0;
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), UTF_8)) {
      return reader.lines().count();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
