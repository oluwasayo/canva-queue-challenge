/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.time.LocalDateTime;
import static java.time.LocalDateTime.parse;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author oladeji
 */
public class MessageTest {

  @Test
  public void testHasExpired() {
    System.out.println("hasExpired");

    Message instance = new Message("1", "Testing microphone", LocalDateTime.MIN);
    assertTrue(instance.hasExpired());

    instance = new Message("1", "Testing microphone", LocalDateTime.MAX);
    assertFalse(instance.hasExpired());

    instance = new Message("1", "Testing microphone", LocalDateTime.MAX);
    assertFalse(instance.hasExpired());

    instance = new Message("1", "Testing microphone", Clock.now().minus(1, ChronoUnit.MILLIS));
    assertTrue(instance.hasExpired());

    instance = new Message("1", "Testing microphone", null);
    assertFalse(instance.hasExpired());
  }

  @Test
  public void testMarkAsRead() {
    System.out.println("markAsRead");

    Message instance = new Message("1", "Testing microphone", LocalDateTime.MAX);
    instance.markAsRead();
    assertNotNull(instance.getReadDate());
    Clock.freeze(LocalDateTime.now().plus(1, ChronoUnit.MILLIS));
    assertTrue(instance.getReadDate().isBefore(Clock.now()));
    Clock.unfreeze();
    assertFalse(instance.isVisible());
  }

  @Test
  public void testMarkAsUnread() {
    System.out.println("markAsUnread");
    Message instance = new Message("1", "Testing microphone", LocalDateTime.MAX);
    instance.markAsRead();
    instance.markAsUnread();
    assertNull(instance.getReadDate());
    assertTrue(instance.isVisible());
  }
}
