package com.example;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author oladeji
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
  Clock.class,
  FileQueueServiceTest.class,
  InMemoryQueueServiceTest.class,
  MessageTest.class,
  SqsQueueServiceTest.class
})
public class ExampleSuite {
}
