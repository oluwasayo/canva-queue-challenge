package com.example;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

/**
 * @author oladeji
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SqsQueueServiceIT {

  private static final String AWS_ACCESS = "awsAccess";
  private static final String AWS_SECRET = "awsSecret";

  private static final String TEST_QUEUE_ID = "DemoQueue";
  private static final String TEST_BODY = "testBody";

  private static final long VISIBILITY_TIMEOUT = 60;

  private static SqsQueueService queueService;

  private static String pulledReceiptHandle;

  @BeforeClass
  public static void setUpClass() throws IOException {
    String accessKey = System.getProperty(AWS_ACCESS);
    String secretKey = System.getProperty(AWS_SECRET);
    if (accessKey == null || secretKey == null) {
      Properties config = new Properties();
      config.load(Files.newBufferedReader(Paths.get("/usr/local/share/sqs.properties"), UTF_8));
      accessKey = config.getProperty(AWS_ACCESS);
      secretKey = config.getProperty(AWS_SECRET);

      if (accessKey == null || secretKey == null) {
        throw new RuntimeException("Cannot run integration test! Ensure that AWS credentials are provided.");
      }
    }

    final BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
    queueService = new SqsQueueService(new AmazonSQSClient(credentials), VISIBILITY_TIMEOUT);
  }

  @Test
  public void t1_testPush() {
    System.out.println("testPush");

    queueService.push(TEST_QUEUE_ID, TEST_BODY, 0);
  }

  @Test
  public void t2_testPull() {
    System.out.println("testPull");

    final Message message = queueService.pull(TEST_QUEUE_ID);
    pulledReceiptHandle = message.getMessageId();
    assertEquals(TEST_BODY,  message.getText());
  }

  @Test
  public void t3_testDelete() {
    System.out.println("testDelete");

    queueService.delete(TEST_QUEUE_ID, pulledReceiptHandle);
  }
}
