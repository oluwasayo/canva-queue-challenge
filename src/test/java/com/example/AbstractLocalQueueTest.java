package com.example;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * @author oladeji
 */
public abstract class AbstractLocalQueueTest {

  @Test
  public void testPush_withLifespan() {
    System.out.println("push_withLifespan");

    getQueueService().push("1", "Testing microphone", 3600000);
    Message message = getQueueService().pull("1");
    assertNotNull(message);
    assertFalse(message.hasExpired());

    getQueueService().delete(message.getQueueId(), message.getMessageId());

    getQueueService().push("1", "Testing microphone", -1);
    message = getQueueService().pull("1");
    assertNull(message);
  }

  @Test
  public void testPush_immortal() {
    System.out.println("push_immortal");

    getQueueService().push("1", "Testing microphone", null);
    Message message = getQueueService().pull("1");
    assertNotNull(message);
    assertFalse(message.hasExpired());
  }

  @Test
  public void testPush_repeatedEntry() {
    System.out.println("push_repeatedEntry");

    getQueueService().push("1", "Testing microphone", null);
    getQueueService().push("1", "Testing microphone", null);
    assertNotNull(getQueueService().pull("1"));
    assertNotNull(getQueueService().pull("1"));
  }

  @Test
  public void testPush_ordering() {
    System.out.println("push_ordering");

    getQueueService().push("1", "Testing microphone1", 3600000);
    getQueueService().push("1", "Testing microphone2", 3600000);
    getQueueService().push("1", "Testing microphone3", 3600000);
    getQueueService().push("1", "Testing microphone4", 3600000);

    assertEquals("Testing microphone1", getQueueService().pull("1").getText());
    assertEquals("Testing microphone2", getQueueService().pull("1").getText());
    assertEquals("Testing microphone3", getQueueService().pull("1").getText());
    assertEquals("Testing microphone4", getQueueService().pull("1").getText());
  }

  @Test
  public void testPull_allExpired() {
    System.out.println("pull_allExpired");

    getQueueService().push("1", "Testing microphone1", -1);
    getQueueService().push("1", "Testing microphone2", -2);
    getQueueService().push("1", "Testing microphone3", -3);

    assertNull(getQueueService().pull("1"));
  }

  @Test
  public void testPull_multipleConcurrentConsumers() {
    System.out.println("pull_multipleConcurrentConsumers");

    for (int a = 1; a <= 50; a++) {
      getQueueService().push("1", "Testing microphone1", 3600000);
    }

    ExecutorService threadPool = Executors.newFixedThreadPool(10);
    List<Future> futures = new ArrayList<>();
    for (int a = 1; a <= 50; a++) {
      futures.add(threadPool.submit(() -> getQueueService().pull("1")));
    }

    futures.stream().forEach(e -> {
      try {
        assertNotNull(e.get());
      } catch (InterruptedException | ExecutionException ex) {
        throw new RuntimeException("Error waiting for multiple consumers to complete.", ex);
      }
    });
  }

  @Test
  public void testPull() {
    System.out.println("pull");

    getQueueService().push("1", "Testing microphone1", 3600000);

    assertEquals("Testing microphone1", getQueueService().pull("1").getText());
    assertNull(getQueueService().pull("1"));
  }

  @Test
  public void testPull_nonExistentQueue() {
    System.out.println("pull_nonExistentQueue");

    getQueueService().push("1", "Testing microphone1", 3600000);

    assertEquals("Testing microphone1", getQueueService().pull("1").getText());
    assertNull(getQueueService().pull("2"));
  }

  @Test
  public void testPull_emptyQueue() {
    System.out.println("pull_emptyQueue");

    getQueueService().push("1", "Testing microphone1", 3600000);
    getQueueService().pull("1");

    assertNull(getQueueService().pull("1"));
  }

  protected abstract QueueService getQueueService();
}
