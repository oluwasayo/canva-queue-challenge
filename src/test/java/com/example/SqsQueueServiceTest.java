package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.amazonaws.services.sqs.model.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author oladeji
 */
public class SqsQueueServiceTest {

  private static final long VISIBILITY_TIMEOUT = 60;

  private static final String DUMMY_URL = "dummyUrl";
  private static final String TEST_QUEUE_ID = "testQueueId";
  private static final String TEST_MESSAGE_ID = "testMessageId";
  private static final String TEST_BODY = "testBody";

  private AmazonSQSClient client;
  private SqsQueueService queueService;

  @Before
  public void setUp() {
    client = mock(AmazonSQSClient.class);
    queueService = spy(new SqsQueueService(client, VISIBILITY_TIMEOUT));
    doReturn(DUMMY_URL).when(queueService).getQueueUrl(anyString());
  }

  @Test
  public void testSpy() {
    System.out.println("testSpy");

    assertEquals(DUMMY_URL, queueService.getQueueUrl("MyQueue"));
  }

  @Test
  public void testPush() {
    System.out.println("testPush");

    final SendMessageRequest request = new SendMessageRequest();
    request.setQueueUrl(DUMMY_URL);
    request.setMessageBody(TEST_BODY);
    final HashMap<String, MessageAttributeValue> attributes = new HashMap<>();
    final MessageAttributeValue attributeValue = new MessageAttributeValue();
    attributeValue.setStringValue("0");
    attributeValue.setDataType("String");
    attributes.put("VisibilityTimeout", attributeValue);
    request.setMessageAttributes(attributes);

    queueService.push("1", TEST_BODY, 0);
    verify(client, times(1)).sendMessage(argThat(new EqualityMatcher<>(request)));
  }

  @Test
  public void testPull() {
    System.out.println("testPull");

    final ReceiveMessageRequest request = new ReceiveMessageRequest();
    request.setQueueUrl(DUMMY_URL);
    request.setMaxNumberOfMessages(1);

    final ReceiveMessageResult response = new ReceiveMessageResult();
    final ArrayList<Message> messages = new ArrayList<>();
    final Message message = new Message();
    message.setMessageId("testId");
    message.setBody(TEST_BODY);
    message.setReceiptHandle("testHandle");
    messages.add(message);
    response.setMessages(messages);

    when(client.receiveMessage(request)).thenReturn(response);
    assertEquals(TEST_BODY, queueService.pull(TEST_QUEUE_ID).getText());
    verify(client, times(1)).receiveMessage(request);
  }

  @Test
  public void testDelete() {
    System.out.println("testDelete");

    queueService.delete(TEST_QUEUE_ID, TEST_MESSAGE_ID);

    DeleteMessageRequest request = new DeleteMessageRequest();
    request.setQueueUrl(DUMMY_URL);
    request.setReceiptHandle(TEST_MESSAGE_ID);
    verify(client, times(1)).deleteMessage(argThat(new EqualityMatcher<>(request)));
  }

  private class EqualityMatcher<T> extends ArgumentMatcher<T> {

    T thisObject;

    private EqualityMatcher(T thisObject) {
      this.thisObject = thisObject;
    }

    @Override
    public boolean matches(Object argument) {
      return thisObject.equals(argument);
    }
  }
}