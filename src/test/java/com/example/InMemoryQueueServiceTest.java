package com.example;

import com.google.common.collect.Iterables;

import java.util.*;
import java.util.concurrent.*;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class InMemoryQueueServiceTest extends AbstractLocalQueueTest {

  private static long VISIBILITY_TIMEOUT = 60;
  private static InMemoryQueueService queueService;

  @BeforeClass
  public static void setUpClass() {
    queueService = new InMemoryQueueService(VISIBILITY_TIMEOUT, false);
  }

  @Before
  public void setUp() {
    queueService.BACKEND.clear();
    queueService.READ_MESSAGES.clear();
  }

  @After
  public void tearDown() {
    Clock.unfreeze();
  }

  @Test
  public void testPush_withExpiry() {
    System.out.println("push_withExpiry");

    assertTrue(queueService.BACKEND.isEmpty());
    queueService.push("1", "Testing microphone", Clock.now().plusMinutes(10));
    assertFalse(queueService.BACKEND.isEmpty());
  }

  @Test
  public void testPush_multipleConcurrentProducers() {
    System.out.println("push_multipleConcurrentProducers");

    ExecutorService threadPool = Executors.newFixedThreadPool(10);
    List<Future> futures = new ArrayList<>();
    Random random = new Random();
    for (int a = 1; a <= 50; a++) {
      final String queueId = Integer.toString(1 + random.nextInt(10));
      futures.add(threadPool.submit(() -> queueService.push(queueId, "Testing mic", 3600000)));
    }

    futures.stream().forEach(e -> {
      try {
        e.get();
      } catch (InterruptedException | ExecutionException ex) {
        throw new RuntimeException("Error waiting for multiple producers to complete.", ex);
      }
    });

    int messageCount = 0;
    messageCount = queueService.BACKEND.values().stream().map(Deque::size).reduce(messageCount, Integer::sum);
    assertEquals(50, messageCount);
  }

  @Test
  public void testDelete() {
    System.out.println("delete");

    queueService.push("1", "Testing microphone1", 3600000);
    final Message message = queueService.pull("1");

    assertNotNull(message);
    assertNull(queueService.pull("1"));
    assertEquals(1, queueService.READ_MESSAGES.size());

    queueService.delete(message.getQueueId(), message.getMessageId());
    assertTrue(queueService.READ_MESSAGES.isEmpty());
  }

  @Test
  public void testDelete_invalidMessage() {
    System.out.println("delete_invalidMessage");

    queueService.push("1", "Testing microphone1", 3600000);
    final Message message = queueService.pull("1");
    assertNotNull(message);
    assertNull(queueService.pull("1"));

    Deque<Message> d1 = new ArrayDeque<>(queueService.READ_MESSAGES);
    queueService.delete(message.getQueueId(), "my random message id");
    Deque<Message> d2 = new ArrayDeque<>(queueService.READ_MESSAGES);

    assertTrue(Iterables.elementsEqual(d1, d2));
  }

  @Test
  public void testAcknowledgement() {
    System.out.println("testAcknowledgement");

    queueService.push("1", "msg1", 3600000);
    queueService.push("1", "msg2", 3600000);
    queueService.push("1", "msg3", 3600000);
    queueService.push("1", "msg4", 3600000);

    Clock.freeze();
    assertEquals("msg1", queueService.pull("1").getText());

    Clock.freeze(Clock.now().plusSeconds(1));
    assertEquals("msg2", queueService.pull("1").getText());

    String queueText = queueService.BACKEND.get("1").stream().map(Message::getText).reduce((e, e1) -> e + e1).get();
    assertEquals("msg3msg4", queueText);
    assertFalse(queueService.READ_MESSAGES.isEmpty());

    Clock.freeze(Clock.now().plusSeconds(VISIBILITY_TIMEOUT + 1));
    queueService.prune();

    queueText = queueService.BACKEND.get("1").stream().map(Message::getText).reduce((e, e1) -> e + e1).get();
    // We only guanrantee that revived messages are pulled before unread messages, we don't guarantee the order of
    // delivery of the revived messages themselves.
    assertTrue(queueText.startsWith("msg1msg2") || queueText.startsWith("msg2msg1"));
    assertTrue(queueText.endsWith("msg3msg4"));
  }

  @Override
  protected QueueService getQueueService() {
    return queueService;
  }
}
