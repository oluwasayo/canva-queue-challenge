package com.example;

import javax.annotation.Nullable;
import java.time.LocalDateTime;

/**
 * An interface representing a queue service. Any implementation (both local and remote) can be done behind this
 * facade.
 *
 * @author oladeji
 */
public interface QueueService {

  /**
   * Sends a single message with a lifespan to a queue. If the queue doesn't exist, it is created (if supported by the
   * backing service) and the message is pushed to it.
   *
   * @param queueId the queue.
   * @param message the textual message.
   * @param lifespan the amount of time in milliseconds before the message expires.
   * @throws UnsupportedOperationException if the backing service does not support on-the-fly queue creation.
   */
  void push(String queueId, String message, long lifespan);

  /**
   * Sends a single message to a queue to expire at a certain time.
   *
   * @param queueId the queue.
   * @param message the textual message.
   * @param expiry the expiry time.
   */
  void push(String queueId, String message, LocalDateTime expiry);

  /**
   * Retrieves a single message from a queue.
   * If the queue does not exist, it is not automatically created as with
   * {@link QueueService#push(String, String, long)} and an implementation <b>may</b> throw a {@link RuntimeException}
   * in that situation.
   * It should be noted that <b>pulling a message does not automatically delete the message from the queue!</b>
   * The message is temporarily made invisible to subsequent pulls until the message is explicitly deleted by the client
   * or the "visibility timeout" elapsed at which point the message is made available again for pulling.
   *
   * @param queueId the queue.
   * @return the message retrieved or {@code null} if the queue is empty.
   */
  @Nullable
  Message pull(String queueId);

  /**
   * Deletes a read message from a queue.
   * If the queue does not exist, it is not automatically created as with
   * {@link QueueService#push(String, String, long)} and an implementation <b>may</b> throw a {@link RuntimeException}
   * in that situation.
   * If the message does not exist on the queue an implementation may throw a {@link RuntimeException} to indicate the
   * situation.
   *
   * @param queueId the queue.
   * @param messageId the message.
   */
  void delete(String queueId, String messageId);
}
