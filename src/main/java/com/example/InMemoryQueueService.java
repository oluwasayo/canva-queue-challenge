package com.example;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;

import java.time.LocalDateTime;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * In-memory implementation of the queue service. This implementation is backed by concurrent non-blocking data
 * structures and uses a background worker that purges these data structures at intervals of the visibility timeout.
 * However dead items do not necessarily linger around till the worker runs, a best attemtp is made to purge them as
 * encoutered during normal operations.
 *
 * @author oladeji
 */
public class InMemoryQueueService implements QueueService {

  @VisibleForTesting
  final Map<String, Deque<Message>> BACKEND;

  @VisibleForTesting
  final Set<Message> READ_MESSAGES;

  private final ScheduledExecutorService PRUNER;
  private final long VISIBILITY_TIMEOUT;

  public InMemoryQueueService(final long visibilityTimeout) {
    this(visibilityTimeout, true);
  }

  @VisibleForTesting
  InMemoryQueueService(final long visibilityTimeout, final boolean createPruner) {
    this.BACKEND = new ConcurrentHashMap<>();
    this.READ_MESSAGES = Sets.newConcurrentHashSet();
    VISIBILITY_TIMEOUT = visibilityTimeout;
    PRUNER = Executors.newSingleThreadScheduledExecutor();
    if (createPruner) {
      PRUNER.scheduleWithFixedDelay(this::prune, VISIBILITY_TIMEOUT, VISIBILITY_TIMEOUT, TimeUnit.SECONDS);
    }
  }

  @Override
  public void push(String queueId, String message, long lifespan) {
    push(queueId, message, Clock.now().plus(lifespan, MILLIS));
  }

  public void push(String queueId, String message, LocalDateTime expiry) {
    if (expiry != null && Clock.now().isAfter(expiry)) {
      return;
    }

    BACKEND.putIfAbsent(queueId, new ConcurrentLinkedDeque<>());
    BACKEND.get(queueId).addLast(new Message(queueId, message, expiry));
  }

  @Override
  public Message pull(String queueId) {
    if (!BACKEND.containsKey(queueId)) {
      return null;
    }

    Message message = null;
    final Iterator<Message> iterator = BACKEND.get(queueId).iterator();
    while(iterator.hasNext()) {
      Message m = iterator.next();
      if (m.hasExpired()) {
        iterator.remove();
        pruneQueue(m);
        continue;
      }

      m.markAsRead();
      READ_MESSAGES.add(m);
      iterator.remove();
      pruneQueue(m);
      message = m;
      break;
    }

    return message;
  }

  @Override
  public void delete(String queueId, String messageId) {
    READ_MESSAGES.remove(new Message(queueId, messageId));
  }

  @Override
  protected void finalize() throws Throwable {
    PRUNER.shutdownNow();
    super.finalize();
  }

  @VisibleForTesting
  void prune() {
    Iterator<Message> iterator = READ_MESSAGES.iterator();
    while (iterator.hasNext()) {
      Message message = iterator.next();
      if (message.hasExpired()) {
        iterator.remove();
        pruneQueue(message);
      } else if (message.getReadDate().until(Clock.now(), SECONDS) >= VISIBILITY_TIMEOUT) {
        message.markAsUnread();
        BACKEND.putIfAbsent(message.getQueueId(), new ConcurrentLinkedDeque<>());
        BACKEND.get(message.getQueueId()).addFirst(message);
      }
    }
  }

  private void pruneQueue(Message message) {
    final Deque<Message> queue = BACKEND.get(message.getMessageId());
    if (queue != null && queue.isEmpty()) {
      BACKEND.remove(message.getMessageId());
    }
  }
}
