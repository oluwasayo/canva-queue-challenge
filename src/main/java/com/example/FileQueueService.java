package com.example;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;

import java.io.*;

import static java.lang.Boolean.parseBoolean;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.*;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Objects.isNull;

import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

// This is a
/**
 * A purely file-based implementation. It is possible to gain performance improvement by caching a working copy of the
 * file in memory and using the file as a backing store or using a memory-mapped file.
 * Sometimes these sort of optimizations require an intimate knowledge of the particular file system, OS, and disk.
 *
 * @author oladeji
 */
public class FileQueueService implements QueueService {

  private static final String RECORD_DELIMITER = "##";

  private final File BASE_DIR;
  private final long VISIBILITY_TIMOUT;

  public FileQueueService(final String baseDir, final long visibilityTimeout) {
    BASE_DIR = new File(baseDir);
    VISIBILITY_TIMOUT = visibilityTimeout;
  }

  @Override
  public void push(String queueId, String message, long lifespan) {
    push(queueId, message, Clock.now().plus(lifespan, MILLIS));
  }

  public void push(String queueId, String message, LocalDateTime expiry) {
    if (expiry != null && Clock.now().isAfter(expiry)) {
      return;
    }

    File messagesFile = getMessagesFile(queueId);
    File lockHandle = getLockHandle(queueId);
    try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(messagesFile.toPath(), UTF_8, CREATE, WRITE, APPEND))) {
      lock(lockHandle);
      pw.println(toRecord(new Message(queueId, message, expiry)));
      pw.flush();
    } catch (InterruptedException ex) {
      throw new RuntimeException("Unable to acquire queue lock. Wait interrupted.", ex);
    } catch (IOException ex) {
      throw new RuntimeException("Unable write output file.", ex);
    } finally {
      unlock(lockHandle);
    }
  }

  @Override
  public Message pull(String queueId) {
    File messagesFile = getMessagesFile(queueId);
    if (!messagesFile.exists()) return null;

    File lockHandle = getLockHandle(queueId);
    final File tempMessagesFile = new File(getQueueDir(queueId), "messages.temp");

    try {
      lock(lockHandle);

      Message result = null;
      try (BufferedReader reader = Files.newBufferedReader(messagesFile.toPath(), UTF_8);
           PrintWriter pw = new PrintWriter(Files.newBufferedWriter(tempMessagesFile.toPath(), UTF_8, CREATE, WRITE))) {

        List<Message> messages = new LinkedList<>();

        String line;
        while ((line = reader.readLine()) != null) {
          if (!line.isEmpty()) {
            messages.add(fromRecord(line)); // It is possible to break early on the first available
            // message, taking note of the position in the file and modifying it using a random seek.
          }
        }

        for (Message m : messages) {
          if (m.isVisible() && (!m.hasExpired())) {
            m.markAsRead();
            result = m;
            break;
          }

          if (m.getReadDate().until(Clock.now(), SECONDS) >= VISIBILITY_TIMOUT) {
            m.markAsUnread(); // Reset state.
            m.markAsRead();
            result = m;
            break;
          }
        }

        messages.stream().map(FileQueueService::toRecord).forEach(pw::println);
        pw.flush();
      } catch (Exception ex) {
        throw new RuntimeException("Unable to perform I/O on queue file.", ex); // Since this fake queue is only used
        // in development on the local machines of engineers, and calls to methods of the in-memory implementations are
        // reliable, it makes sense not to pollute the API with checked exception throw clauses. The only things we're
        // worried about here is file permissions and corruption. The developer can ensure this doesn't happen on his
        // machine.
      }

      messagesFile.delete();
      tempMessagesFile.renameTo(messagesFile);

      return result;
    } catch (InterruptedException ex) {
      throw new RuntimeException("Unable to acquire queue lock. Wait interrupted.", ex);
    } finally {
      unlock(lockHandle);
    }
  }

  @Override
  // It is possible to speed this operation up by taking advantage of the push and pull operations to index the position
  // where each messageId is in a hashtable in memory, DELETE would then only be a matter of random seek.
  public void delete(String queueId, String messageId) {
    File messagesFile = getMessagesFile(queueId);
    File lockHandle = getLockHandle(queueId);
    final File tempMessagesFile = new File(getQueueDir(queueId), "messages.temp");

    try {
      lock(lockHandle);

      try (Scanner sc = new Scanner(messagesFile);
          PrintWriter pw = new PrintWriter(new FileWriter(tempMessagesFile, false))) {
        while (sc.hasNextLine()) {
          Message message = fromRecord(sc.nextLine());
          if (message.getMessageId().equals(messageId)) {
            continue;
          }
          pw.println(toRecord(message));
        }

        pw.flush();
      } catch (Exception ex) {
        throw new RuntimeException("Unable to perform I/O on queue file.", ex);
      }

      messagesFile.delete();
      tempMessagesFile.renameTo(messagesFile);
    } catch (InterruptedException ex) {
      throw new RuntimeException("Unable to acquire queue lock. Wait interrupted.", ex);
    } finally {
      unlock(lockHandle);
    }
  }

  @VisibleForTesting
  void lock(File lockHandle) throws InterruptedException {
      while (!lockHandle.mkdir()) {
        Thread.sleep(50);
      }
  }

  @VisibleForTesting
  void unlock(File lockHandle) {
    lockHandle.delete();
  }

  @VisibleForTesting
  File getQueueDir(String queueId) {
    final File queueDir = new File(BASE_DIR, queueId);
    if (!queueDir.exists()) queueDir.mkdirs();
    return queueDir;
  }

  @VisibleForTesting
  File getMessagesFile(String queueId) {
    return new File(getQueueDir(queueId), "messages");
  }

  @VisibleForTesting
  File getLockHandle(String queueId) {
    return new File(getQueueDir(queueId), ".lock");
  }

  @VisibleForTesting
  static Message fromRecord(String record) {
    if (record == null) {
      return null;
    }

    String[] tokens = record.split(RECORD_DELIMITER, 6);
    if (tokens.length < 6) {
      throw new IllegalArgumentException("Malformed record! Tokens[0]=" + tokens[0] + ".");
    }

    LocalDateTime expiry = "null".equals(tokens[2]) ? null : LocalDateTime.parse(tokens[2], ISO_LOCAL_DATE_TIME);
    LocalDateTime readDate = "null".equals(tokens[4]) ? null : LocalDateTime.parse(tokens[4], ISO_LOCAL_DATE_TIME);
    return new Message(tokens[0], tokens[1], tokens[5], expiry, parseBoolean(tokens[3]), readDate);
  }

  @VisibleForTesting
  static String toRecord(Message m) {
    return Joiner.on(RECORD_DELIMITER).useForNull("null").join(m.getQueueId(), m.getMessageId(),
        m.getExpiry() == null ? "null" : m.getExpiry().format(ISO_LOCAL_DATE_TIME), m.isVisible(),
        m.getReadDate() == null ? "null" : m.getReadDate().format(ISO_LOCAL_DATE_TIME), m.getText());
  }
}
