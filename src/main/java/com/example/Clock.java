package com.example;

import com.google.common.annotations.VisibleForTesting;

import java.time.LocalDateTime;

/**
 * Allows us to manipulate the clock so that tests don't have to sleep threads.
 *
 * @author oladeji
 */
public class Clock {

  private static LocalDateTime frozenTime;

  private Clock() {
  }

  public static LocalDateTime now() {
    return isFrozen() ? frozenTime : LocalDateTime.now();
  }

  public static LocalDateTime freeze() {
    return freeze(LocalDateTime.now());
  }

  public static LocalDateTime freeze(LocalDateTime frozenTime) {
    Clock.frozenTime = frozenTime == null ? LocalDateTime.MIN : frozenTime;
    return Clock.frozenTime;
  }

  public static void unfreeze() {
    frozenTime = null;
  }

  @VisibleForTesting
  static boolean isFrozen() {
    return frozenTime != null;
  }
}
