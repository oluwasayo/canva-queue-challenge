package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.google.common.annotations.VisibleForTesting;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SqsQueueService implements QueueService {

  private final AmazonSQSClient SQS;
  private final long VISIBILITY_TIMOUT;

  public SqsQueueService(final AmazonSQSClient sqsClient, final long visibilityTimout) {
    this.SQS = sqsClient;
    this.VISIBILITY_TIMOUT = visibilityTimout;
  }

  @Override
  public void push(String queueId, String message, long lifespan) {
    final SendMessageRequest request = new SendMessageRequest();
    try {
      request.setQueueUrl(getQueueUrl(queueId));
    } catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException ex) {
      SQS.createQueue(queueId);
      request.setQueueUrl(getQueueUrl(queueId));
    }

    request.setMessageBody(message);
    final HashMap<String, MessageAttributeValue> attributes = new HashMap<>();
    final MessageAttributeValue attributeValue = new MessageAttributeValue();
    attributeValue.setStringValue(Long.toString(lifespan / 1000));
    attributeValue.setDataType("String");
    attributes.put("VisibilityTimeout", attributeValue);
    request.setMessageAttributes(attributes);

    SQS.sendMessage(request);
  }

  /**
   * {@inheritDoc}
   * The lifespan is calculated using the current system
   * time and used with {@link this#push(String, String, long)}.
   *
   * @param queueId the queue.
   * @param message the textual message.
   * @param expiry the expiry time.
   */
  public void push(String queueId, String message, LocalDateTime expiry) {
    push(queueId, message, Clock.now().until(expiry, ChronoUnit.SECONDS));
  }

  @Override
  public Message pull(String queueId) {
    final ReceiveMessageRequest request = new ReceiveMessageRequest();
    request.setQueueUrl(getQueueUrl(queueId));
    request.setMaxNumberOfMessages(1);

    final List<com.amazonaws.services.sqs.model.Message> messages = SQS.receiveMessage(request).getMessages();
    if (messages.isEmpty()) return null;
    final com.amazonaws.services.sqs.model.Message awsMessage = messages.get(0);
    return new Message(queueId, awsMessage.getReceiptHandle(), awsMessage.getBody(), Clock.now(), true, Clock.now());
  }

  @Override
  public void delete(String queueId, String messageId) {
    DeleteMessageRequest request = new DeleteMessageRequest();
    request.setQueueUrl(getQueueUrl(queueId));
    request.setReceiptHandle(messageId);

    SQS.deleteMessage(request);
  }

  @VisibleForTesting
  String getQueueUrl(String queueId) {
    return SQS.getQueueUrl(new GetQueueUrlRequest(queueId)).getQueueUrl();
  }
}
