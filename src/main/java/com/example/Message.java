package com.example;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.hash.Hashing;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author oladeji
 */
public class Message implements Comparable<Message> {

  private String queueId;
  private String messageId;
  private String text;
  private LocalDateTime expiry;
  private boolean visible;
  private LocalDateTime readDate;

  public Message(final String queueId, final String text, final LocalDateTime expiry) {
    this(queueId, hash(queueId, text), text, expiry, true, null);
  }

  /**
   * Only for use when making identity comparisons. This speeds up deletes. This isn't public because it doesn't make
   * sense to have it in the API.
   *
   * @param messageId messageId as received from @{code Message#pull}.
   */
  Message(final String messageId) {
    this(null, messageId);
  }

  /**
   * Only for use when making identity comparisons. This speeds up deletes. This isn't public because it doesn't make
   * sense to have it in the API.
   *
   * @param messageId messageId as received from @{code Message#pull}.
   * @param queueId the queue the message belongs to.
   */
  Message(final String queueId, final String messageId) {
    this(queueId, messageId, null, null, true, null);
  }

  @VisibleForTesting
  Message(String queueId, String messageId, String text, LocalDateTime expiry, boolean visible,
      LocalDateTime readDate) {
    this.queueId = queueId;
    this.messageId = messageId;
    this.text = text;
    this.expiry = expiry == null ? LocalDateTime.MAX : expiry;
    this.visible = visible;
    this.readDate = readDate;
  }

  private static String hash(String... input) {
    return Hashing.md5().hashString(Joiner.on('|').join(input), Charset.forName("UTF-8")).toString();
  }

  private static boolean isNull(String token) {
    return token == null || token.isEmpty() || "null".equalsIgnoreCase(token);
  }

  public String getQueueId() {
    return queueId;
  }

  public String getMessageId() {
    return messageId;
  }

  public String getText() {
    return text;
  }

  public boolean hasExpired() {
    return this.expiry != null && this.expiry.isBefore(Clock.now());
  }

  public LocalDateTime getExpiry() {
    return this.expiry;
  }

  public boolean isVisible() {
    return visible;
  }

  public LocalDateTime getReadDate() {
    return readDate;
  }

  public void markAsRead() {
    this.readDate = Clock.now();
    this.visible = false;
  }

  public void markAsUnread() {
    this.readDate = null;
    this.visible = true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 29 * hash + Objects.hashCode(this.messageId);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    final Message other = (Message) obj;
    return Objects.equals(this.messageId, other.messageId);
  }

  @Override
  public int compareTo(Message o) {
    return o == null ? 1 : this.messageId.equals(o.messageId) ? 0 : this.messageId.compareTo(o.messageId);
  }

  @Override
  public String toString() {
    return "Message{" +
        "queueId='" + queueId + '\'' +
        ", messageId='" + messageId + '\'' +
        ", text='" + text + '\'' +
        ", expiry=" + expiry +
        ", visible=" + visible +
        ", readDate=" + readDate +
        '}';
  }
}
